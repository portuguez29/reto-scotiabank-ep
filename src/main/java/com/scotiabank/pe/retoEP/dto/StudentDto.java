package com.scotiabank.pe.retoEP.dto;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto implements Serializable {

    @Min(value = 0, message = "El id tiene que ser mayor que cero")
    private Integer id;
    @NotBlank(message = "El nombre no puede estar en blanco")
    private String name;
    @NotBlank(message = "El apellido no puede estar en blanco")
    private String lastName;
    @NotNull(message = "El estado no puede ser null")
    private String state;
    @NotNull(message = "La edad no puede ser null")
    @Min(value = 1, message = "La edad tiene que ser mayor que cero")
    private Integer age;

}
