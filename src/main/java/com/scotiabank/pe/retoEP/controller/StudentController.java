package com.scotiabank.pe.retoEP.controller;

import com.scotiabank.pe.retoEP.dto.StudentDto;
import com.scotiabank.pe.retoEP.model.Student;
import com.scotiabank.pe.retoEP.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/student")
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/test")
    public String test(){
        return "Testing Successful.";
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Validated
    public Mono<Void> saveStudent(@Valid @RequestBody StudentDto param) {
        return studentService.saveStudent(param);
    }

    @GetMapping("/active")
    public Flux<StudentDto> getActiveStudents() {
        return studentService.getActiveStudents();
    }

    @GetMapping("/all")
    public Flux<Student> getAllStudents() {
        return studentService.getAllStudents();
    }


}
