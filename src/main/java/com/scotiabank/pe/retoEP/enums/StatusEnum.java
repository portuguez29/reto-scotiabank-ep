package com.scotiabank.pe.retoEP.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.EnumSet;

@Getter
@ToString
@AllArgsConstructor
public enum StatusEnum {

    ACTIVE("1","activo"),
    INACTIVE("0","inactivo");

    private final String cod;
    private final String desc;

    private static final Map<String, StatusEnum> lookup = new LinkedHashMap<>();

    static {
        for (StatusEnum s : EnumSet.allOf(StatusEnum.class))
            lookup.put(s.getCod(), s);
    }

    public static StatusEnum findByCod(String codigo) {
        return lookup.get(codigo);
    }

    public static List<StatusEnum> findAll() {
        return new ArrayList<>(lookup.values());
    }

}
