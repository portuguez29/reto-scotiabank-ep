package com.scotiabank.pe.retoEP.repository;

import com.scotiabank.pe.retoEP.model.Student;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface StudentRepository extends R2dbcRepository<Student, Integer> {
    @Query("select * from student where state = :state")
    Flux<Student> findByState(String state);

}
