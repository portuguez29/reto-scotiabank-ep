package com.scotiabank.pe.retoEP.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @org.springframework.data.annotation.Id
    private Integer id;
    private String name;
    private String lastName;
    private String state;
    private Integer age;
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone ="GMT-5:00")
    @Column(name="created",  nullable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime created;
    @UpdateTimestamp
    @Column(name="modified", insertable = false, columnDefinition="TIMESTAMP NULL")
    private LocalDateTime modified;
    @Column(name="deleted", columnDefinition="TIMESTAMP NULL")
    private LocalDateTime deleted;

}
