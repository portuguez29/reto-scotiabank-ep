package com.scotiabank.pe.retoEP.mapper;

import com.scotiabank.pe.retoEP.dto.StudentDto;
import com.scotiabank.pe.retoEP.model.Student;

public class StudentMapper {

    public static StudentDto mapToStudentDto(Student student){
        return StudentDto.builder()
                .id(student.getId())
                .name(student.getName())
                .lastName(student.getLastName())
                .state(student.getState())
                .age(student.getAge())
                .build();
    }

    public static Student mapToStudent(StudentDto studentDto){
        return Student.builder()
                .name(studentDto.getName())
                .lastName(studentDto.getLastName())
                .state(studentDto.getState())
                .age(studentDto.getAge())
                .build();
    }

}
