package com.scotiabank.pe.retoEP.service.impl;

import com.scotiabank.pe.retoEP.dto.StudentDto;
import com.scotiabank.pe.retoEP.enums.StatusEnum;
import com.scotiabank.pe.retoEP.mapper.StudentMapper;
import com.scotiabank.pe.retoEP.model.Student;
import com.scotiabank.pe.retoEP.repository.StudentRepository;
import com.scotiabank.pe.retoEP.service.StudentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@AllArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    @Override
    public Mono<Void> saveStudent(StudentDto param) {
        log.info("Init saveStudent -> {}", param);

        return studentRepository.existsById(param.getId())
                .flatMap(exists -> {
                    if (exists) {
                        return Mono.error(new ResponseStatusException(HttpStatus.CONFLICT, "El alumno con el ID proporcionado ya existe. No se pudo realizar la grabación."));
                    } else {
                        log.info("Save Student");
                        Student studentToSave = StudentMapper.mapToStudent(param);
                        return studentRepository.save(studentToSave);
                    }
                }).then();
    }

    @Override
    public Flux<StudentDto> getActiveStudents() {
        log.info("Init getActiveStudents");
        return studentRepository.findByState(StatusEnum.ACTIVE.getDesc())
                .map(StudentMapper::mapToStudentDto)
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NO_CONTENT, "No se encontraron estudiantes activos")));
    }

    @Override
    public Flux<Student> getAllStudents() {
        log.info("Init getAllStudents");
        return studentRepository.findAll();
    }

}
