package com.scotiabank.pe.retoEP.service;

import com.scotiabank.pe.retoEP.dto.StudentDto;
import com.scotiabank.pe.retoEP.model.Student;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface StudentService {

    Mono<Void> saveStudent(StudentDto student);
    Flux<StudentDto> getActiveStudents();
    Flux<Student> getAllStudents();

}
