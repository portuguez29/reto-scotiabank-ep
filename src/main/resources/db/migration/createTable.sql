CREATE TABLE IF NOT EXISTS student
(
    id  INT  NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name  VARCHAR(255) NOT NULL,
    last_name VARCHAR(255),
    state VARCHAR(10) NOT NULL,
    age INT NOT NULL,
    created  timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp NULL,
    deleted timestamp NULL
);