package com.scotiabank.pe.retoEP;

import com.scotiabank.pe.retoEP.controller.StudentController;
import com.scotiabank.pe.retoEP.dto.StudentDto;
import com.scotiabank.pe.retoEP.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@Slf4j
class RetoEpApplicationTests {

	@InjectMocks
	private StudentController studentController;

	@Mock
	private StudentService studentService;


	@Test
	void saveStudentTest() {
		StudentDto studentDto = StudentDto.builder()
				.name("Juan")
				.lastName("Perez")
				.state("activo")
				.age(25)
				.build();

		when(studentService.saveStudent(eq(studentDto))).thenReturn(Mono.empty());

		WebTestClient
				.bindToController(studentController)
				.build()
				.post()
				.uri("/api/student")
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(studentDto)
				.exchange()
				.expectStatus().isCreated()
				.expectBody().isEmpty();
	}

	@Test
	void getActiveStudentsTest() {
		when(studentService.getActiveStudents()).thenReturn(Flux.empty());

		StepVerifier.create(studentController.getActiveStudents())
				.expectNextCount(0)
				.expectComplete()
				.verify();

		verify(studentService).getActiveStudents();
	}

	@Test
	void getAllStudentsTest() {
		when(studentService.getAllStudents()).thenReturn(Flux.empty());

		StepVerifier.create(studentController.getAllStudents())
				.expectNextCount(0)
				.expectComplete()
				.verify();

		verify(studentService).getAllStudents();
	}

}
