FROM openjdk:11-jre-slim
COPY "./target/retoEP-0.0.1-SNAPSHOT.jar" "retoscotiabank.jar"
ENTRYPOINT ["java","-jar","retoscotiabank.jar"]